const items = [1, 2, 3, 4, 5, 5];

function each(elements, cb) {
    if(!Array.isArray(elements)) return [];
    for(let idx = 0; idx < elements.length; idx++){
        cb(elements[idx],idx);
    }
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
}
module.exports = each;
function reduce(elements, cb, startingValue) {
    let flag = false;
    if(startingValue === undefined){
        startingValue = elements[0];
        flag = true;
    }
    for(let idx=0; idx<elements.length; idx++){
        if (flag === true){
            flag = false;
            continue;
        }
        
        startingValue =  cb(startingValue,elements[idx],idx,elements);
    }
    return startingValue;
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
}
module.exports = reduce;

let arr = [];
function flatten(elements, depth=1) {
    for(let idx=0; idx < elements.length; idx++){
        if(Array.isArray(elements[idx]) && depth>0){
            flatten(elements[idx], depth-1);
        }else if(elements[idx] !== undefined){
            arr.push(elements[idx]);
        }
    }
    return arr;
    }
    module.exports = flatten;
// console.log(flatten([1, [2], [3, [[4]]]]));

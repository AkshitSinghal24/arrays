function map(elements, cb) {
    const arr=[];
    elements.forEach((element,idx,newArr) => {
        const el = cb(element,idx,newArr);
        arr.push(el);
    });
    return arr;
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
}
module.exports = map;

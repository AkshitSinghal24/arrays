function filter(elements, cb) {
    const arr=[];
    elements.forEach((element,idx,newArr) => {
        const check = cb(element,idx,newArr);
        if(check === true) arr.push(element);
    });
    return arr;
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
}
module.exports = filter;
function find(elements, cb) {
    elements.forEach(element => {
        const check = cb(element);
        if(check) return element;
        else return undefined;
    });
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
}
module.exports = find;